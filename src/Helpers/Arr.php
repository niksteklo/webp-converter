<?php

namespace Root\WebpConverter\Helpers;

class Arr
{
    public static function first(array $array): mixed
    {
        return $array[0] ?? null;
    }
    public static function last(array $array): mixed
    {
        return $array[array_key_last($array)];;
    }
}
