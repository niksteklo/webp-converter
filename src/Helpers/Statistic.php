<?php

namespace Root\WebpConverter\Helpers;

class Statistic
{
    private static int $convertedImagesCount = 0;

    private static int $downloadedImagesCount = 0;

    private static float $start;

    public static function startExecution(): void
    {
        self::$start = microtime(true);
    }

    public static function getExecutionTime(): string
    {
        return number_format(microtime(true) - self::$start, 2);
    }

    public static function getConvertedImagesCount(): int
    {
        return self::$convertedImagesCount;
    }

    public static function incrementConvertedImage(): void
    {
        self::$convertedImagesCount++;
    }

    public static function getDownloadedImagesCount(): int
    {
        return self::$downloadedImagesCount;
    }

    public static function incrementDownloadedImage(): void
    {
        self::$downloadedImagesCount++;
    }
}
