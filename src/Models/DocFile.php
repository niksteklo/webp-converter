<?php

namespace Root\WebpConverter\Models;

use Root\WebpConverter\Exceptions\WrongCsvFile;

class DocFile extends AbstractFile
{
    public const ALLOWED_FILE_TYPE = 'csv';

    public function isCsv(): bool
    {
        return strtolower($this->extension) === self::ALLOWED_FILE_TYPE;
    }

    /**
     * @throws WrongCsvFile
     */
    public function getRows(): iterable
    {
        $filePath = $this->getImportCsvFilePath();

        $openFile = fopen($filePath, 'rb');

        while (($data = fgetcsv($openFile, 1000, ',')) !== false) {
            if (count($data) > 1 || count($data) === 0) {
                throw new WrongCsvFile(
                    'В csv файле должна быть 1 колонка: ' . json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
                );
            }

            yield $data[0];
        }

        fclose($openFile);
    }

    public function getImportCsvFilePath(string $fileName = null): string
    {
        return $this->dir->getImportCsvFilePath($fileName ?: $this->fileName, $this->extension);
    }

    public function deleteFromImportCsvDir(string $fileName = null): void
    {
        unlink($this->getImportCsvFilePath($fileName));
    }
}
