<?php

namespace Root\WebpConverter\Models;

class Dir
{
    public const IMPORT_CSV_DIR = './import_csv';

    public const IMPORT_DIR = './import';

    public const EXPORT_DIR = './export';

    public const RESIDUAL_FILE_EXTENSIONS = ['.Identifier'];

    public const IGNORED_FILES_IN_DIRS = ['.', '..', '.gitkeep'];

    public function getFilesFromDir(string $dirPath): array
    {
        $fileNames = scandir($dirPath);

        return array_filter(
            $fileNames,
            static fn (string $fileName) => !in_array($fileName, self::IGNORED_FILES_IN_DIRS, true)
        );
    }

    public function getImportCsvFilePath(string $fileName, string $extension): string
    {
        return self::IMPORT_CSV_DIR.'/'.$fileName.'.'.$extension;
    }

    public function getImportFilePath(string $fileName, string $extension): string
    {
        return self::IMPORT_DIR.'/'.$fileName.'.'.$extension;
    }

    public function getExportFilePath(string $fileName, string $extension): string
    {
        return self::EXPORT_DIR.'/'.$fileName.'.'.$extension;
    }

    public function removeResidualFiles(): void
    {
        foreach ($this->getFilesFromDir(self::IMPORT_DIR) as $fileName) {
            foreach (self::RESIDUAL_FILE_EXTENSIONS as $extension) {
                if (str_contains($fileName, $extension)) {
                    unlink(self::IMPORT_DIR.'/'.$fileName);
                }
            }
        }
    }
}
