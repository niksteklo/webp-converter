<?php

namespace Root\WebpConverter\Models;

use Root\WebpConverter\Helpers\Statistic;

class ImageFile extends AbstractFile
{
    public const TARGET_IMAGE_TYPE = 'webp';

    public function prepareForConvert(): void
    {
        foreach (self::FORBIDDEN_SYMBOLS_IN_FILE_NAME as $forbiddenSymbol) {
            if (str_contains($this->fileName, $forbiddenSymbol)) {
                $fixedFile = $this->getFixedFileName($this->fileName);

                $this->rename($fixedFile);

                $this->fileName = $fixedFile;

                break;
            }
        }
    }

    public function convert(): void
    {
        shell_exec(
            sprintf(
            'cwebp %s -o %s -quiet',
            $this->getImportFilePath(),
            $this->getExportFilePath()
            )
        );

        Statistic::incrementConvertedImage();
    }

    public function getExportFilePath(string $fileName = null): string
    {
        return $this->dir->getExportFilePath($fileName ?: $this->fileName, self::TARGET_IMAGE_TYPE);
    }
}
