<?php

namespace Root\WebpConverter\Models;

use Root\WebpConverter\Exceptions\FailedImageDownload;
use Root\WebpConverter\Helpers\Statistic;

class DownloadFile extends AbstractFile
{
    public const LINKS_SEPARATOR = ',';

    public static function parseLink(string $link): array
    {
        return explode(self::LINKS_SEPARATOR, $link);
    }

    /**
     * @throws FailedImageDownload
     */
    public function download(): void
    {
        $newFileName = $this->getFileName(true);

        $imageData = file_get_contents($this->getFullFileName());

        if ($imageData === false) {
            throw new FailedImageDownload('Не удалось загрузить картинку: ' . $this->getFullFileName());
        }

        file_put_contents($this->dir->getImportFilePath($newFileName, $this->extension), $imageData);

        Statistic::incrementDownloadedImage();
    }
}
