<?php

namespace Root\WebpConverter\Models;

use Root\WebpConverter\Helpers\Arr;

abstract class AbstractFile
{
    public const FORBIDDEN_SYMBOLS_IN_FILE_NAME = [' ', '(', ')'];

    public const ALLOWED_IMAGE_TYPES = ['jpeg', 'jpg', 'png', 'svg'];

    protected string $extension;

    protected string $fileName;

    public function __construct(protected readonly Dir $dir, string $fileName)
    {
        $fileName = trim($fileName);
        $explodedFileName = explode('.', $fileName);

        $this->extension = array_pop($explodedFileName);
        $this->fileName = implode('.', $explodedFileName);
    }

    public function isImage(): bool
    {
        return in_array(strtolower($this->extension), self::ALLOWED_IMAGE_TYPES, true);
    }

    public function getFullFileName(): string
    {
        return $this->fileName . '.' . $this->extension;
    }

    public function deleteFromImportDir(string $fileName = null): void
    {
        unlink($this->getImportFilePath($fileName));
    }

    public function getImportFilePath(string $fileName = null): string
    {
        return $this->dir->getImportFilePath($fileName ?: $this->fileName, $this->extension);
    }

    protected function getFixedFileName(string $fileName): string
    {
        return str_replace(self::FORBIDDEN_SYMBOLS_IN_FILE_NAME, '_', $fileName);
    }

    protected function rename(string $newFileName): void
    {
        rename($this->getImportFilePath(), $this->getImportFilePath($newFileName));
    }

    protected function getFileName(bool $isWithoutExtension = false): string
    {
        $fileName = Arr::last(explode('/', $this->getFullFileName()));

        return $isWithoutExtension
            ? Arr::first(explode('.', $fileName))
            : $fileName;
    }
}
