<?php

use Root\WebpConverter\Exceptions\FailedImageDownload;
use Root\WebpConverter\Exceptions\WrongCsvFile;
use Root\WebpConverter\Helpers\Statistic;
use Root\WebpConverter\Models\Dir;
use Root\WebpConverter\Models\DocFile;
use Root\WebpConverter\Models\DownloadFile;
use Root\WebpConverter\Models\ImageFile;

require_once './vendor/autoload.php';

Statistic::startExecution();

$dir = new Dir();

foreach ($dir->getFilesFromDir(Dir::IMPORT_CSV_DIR) as $fileName) {
    dump(sprintf('Загрузка изображений из файла "%s"', $fileName));

    $file = new DocFile($dir, $fileName);

    if (!$file->isCsv()) {
        continue;
    }

    try {
        foreach ($file->getRows() as $row) {
            foreach (DownloadFile::parseLink($row ?? '') as $link) {
                $downloadFile = new DownloadFile($dir, $link);

                if (!$downloadFile->isImage()) {
                    continue;
                }

                try {
                    $downloadFile->download();
                } catch (FailedImageDownload $e) {
                    dump($e->getMessage());
                    continue;
                }
            }
        }
    } catch (WrongCsvFile $e) {
        dump($e->getMessage());
    }

    $file->deleteFromImportCsvDir();
}

if ($downloadedImagesCount = Statistic::getDownloadedImagesCount()) {
    dump(sprintf('Количество скачанных изображений: %s шт.', $downloadedImagesCount));
}

dump('Начало конвертации...');

foreach ($dir->getFilesFromDir(Dir::IMPORT_DIR) as $fileName) {
    $image = new ImageFile($dir, $fileName);

    if (!$image->isImage()) {
        continue;
    }

    $image->prepareForConvert();

    $image->convert();

    $image->deleteFromImportDir();
}

$dir->removeResidualFiles();

dump(sprintf(
    'Конвертация окончена! Конвертация заняла: %s сек. Кол-во конвертированных изображений: %s шт.',
    Statistic::getExecutionTime(),
    Statistic::getConvertedImagesCount(),
));
